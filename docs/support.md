# Support

Please report any issues on [Gitlab](https://gitlab.com/hexgear/seven-sun-downs/seven-sun-downs/-/issues)

## See Also

- [Upstream Bug Tracker](https://github.com/ryzom/ryzomcore/issues)
