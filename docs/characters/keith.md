---
category: character
age: 26
role: combat
gender: male
status: living
height: 6'
weight: 180
hair: green
eyes: brown
faction: icc
---

# Keith

_Serving as a vanguard for several years prior, he originally joined as
part of the security detail for the initial research party. Keith
successfully defended himself, as well as [Penny](../penny), from the
harsh environment of the planet, single-handedly, after the rest of the 
research team met their demise._
