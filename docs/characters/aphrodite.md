---
category: character
gender: female
status: living
faction: neutral
---

# Aphrodite

_Taken captive near the wreckage of the research team's ship,
she is more than willing to do whatever it takes in exchange for her freedom._
