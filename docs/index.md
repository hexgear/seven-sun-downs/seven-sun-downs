# Seven Sun Downs

Seven Sun Downs is a science fiction role playing game that is currently 
in development by [HexGear Studio](https://hexgear.gitlab.io).

## Synopsis

Employed by the Interplanetary Colonization Company (ICC), your job is to support
the development of new settlements on foreign planets. However, a research 
party, sent to investigate a new planet has gone missing. You have been assigned
to be part of makeshift rescue squad to retrieve the missing researchers. But as
your squad is making planetfall, your shuttle passes through an electric storm,
crippling your shuttle, which must attempt an emergency crash landing. Unfortunately,
it appears you have survived the crash, only to find half of your squad dead. Now
it is up to you and the other survivors to both find the missing research party,
as well as find a way off planet. But this hostile planet is not about to let that
happen. Facing off against natural predatory species, as well as what appears to be
an indigenous humanoid population, your squad also struggles to brave the strange
climate, as well as interpersonal issues that have begun to manifest amongst yourselves.
And you only have seven days to do it. Within seven days, the planet will go through
an eclipse, which you have been informed is nearly impossible to survive.
When the sun sets on the seventh day, you and your squad will simply cease to exist.
Seven days to save the research team and yourselves. Seven Sun Downs. 
